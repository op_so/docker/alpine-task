# Docker alpine task

[![Software License](https://img.shields.io/badge/license-MIT-informational.svg?style=flat)](LICENSE)
[![Pipeline Status](https://gitlab.com/op_so/docker/alpine-task/badges/main/pipeline.svg)](https://gitlab.com/op_so/docker/alpine-task/pipelines)

A [Task](https://taskfile.dev/) Docker image:

* **lightweight** image based on Alpine Linux only 11 MB,
* `multiarch` with support of **amd64** and **arm64**,
* **non-root** container user,
* **automatically** updated by comparing SBOM changes,
* image **signed** with [Cosign](https://github.com/sigstore/cosign),
* an **SBOM attestation** added using [`Syft`](https://github.com/anchore/syft),
* available on **Docker Hub** and **Quay.io**.

[![GitLab](https://shields.io/badge/Gitlab-informational?logo=gitlab&style=flat-square)](https://gitlab.com/op_so/docker/alpine-task) The main repository.

[![Docker Hub](https://shields.io/badge/dockerhub-informational?logo=docker&logoColor=white&style=flat-square)](https://hub.docker.com/r/jfxs/alpine-task) The Docker Hub registry.

[![Quay.io](https://shields.io/badge/quay.io-informational?logo=docker&logoColor=white&style=flat-square)](https://quay.io/repository/ifxs/alpine-task) The Quay.io registry.

## Running task

```shell
docker run -t --rm jfxs/alpine-task task --version
```

or

```shell
docker run -t --rm quay.io/ifxs/alpine-task task --version
```

## Built with

Docker latest tag is [--VERSION--](https://gitlab.com/op_so/docker/alpine-task/-/blob/main/Dockerfile) and contains:

--SBOM-TABLE--

Details are updated on [`Dockerhub` Overview page](https://hub.docker.com/r/jfxs/alpine-task) when an image is published.

## Versioning

The Docker tag is defined by the Task version used and an increment to differentiate build with the same Task version:

```text
<task_version>-<increment>
```

<!-- vale off -->
Example: 3.19.1-003
<!-- vale on -->

## Signature and attestation

[Cosign](https://github.com/sigstore/cosign) public key:

```shell
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEa3yV6+yd/l4zh/tfT6Tx+zn0dhy3
BhFqSad1norLeKSCN2MILv4fZ9GA6ODOlJOw+7vzUvzZVr9IXnxEdjoWJw==
-----END PUBLIC KEY-----
```

The public key is also available online: <https://gitlab.com/op_so/docker/cosign-public-key/-/raw/main/cosign.pub>.

To verify an image:

```shell
cosign verify --key cosign.pub $IMAGE_URI
```

To verify and get the SBOM attestation:

```shell
cosign verify-attestation --key cosign.pub --type spdxjson $IMAGE_URI | jq '.payload | @base64d | fromjson | .predicate'
```

## Authors

<!-- vale off -->
* **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)
<!-- vale on -->

## License

<!-- vale off -->
This program is free software: you can redistribute it and/or modify it under the terms of the MIT License (MIT). See the [LICENSE](https://opensource.org/licenses/MIT) for details.
<!-- vale on -->
